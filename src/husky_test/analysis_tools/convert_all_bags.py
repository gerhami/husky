#!/usr/bin/python

import rosbag, sys, csv
import time
import string
import os #for file management make directory
import shutil #for file management, copy file
import rosbag_to_csv

listOfBagFiles = [f for f in os.listdir(".") if f[-4:] == ".bag"]	#get list of only bag files in current dir.
numberOfFiles = str(len(listOfBagFiles))
print "reading all " + numberOfFiles + " bagfiles in data directory: \n"

count = 0
for bagFile in listOfBagFiles:
    count += 1
    print(bagFile)
    print("reading file " + str(count) + " of  " + numberOfFiles + ": " + bagFile)
    os.system("python ../analysis_tools/rosbag_to_csv.py -b ../data/" + bagFile)
    print("\n")
print "Done reading all " + numberOfFiles + " bag files."
