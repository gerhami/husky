#include "movebase.h"

TEST_F(MovebaseTest, testGoalReached) {
  geometry_msgs::PoseStamped goal;

  goal.header.stamp = ros::Time::now();
  goal.header.frame_id = "odom";
  goal.pose.position.x = 5.395;
  goal.pose.position.y = -6.893;
  goal.pose.position.z = 0.0;
  goal.pose.orientation.x = 0.0;
  goal.pose.orientation.y = 0.0;
  goal.pose.orientation.z = -0.446;
  goal.pose.orientation.z = 0.895;

  publish_goal(goal);
  ros::Duration(25.0).sleep();

  nav_msgs::Odometry pose = getLastStatus();
  EXPECT_NEAR(pose.pose.pose.position.x, 5.395, POSE_TOLERANCE);
  EXPECT_NEAR(pose.pose.pose.position.y, -6.893, POSE_TOLERANCE);
}

void parser() {
  // Nothing yet
}

int main(int argc, char** argv) {
  // Parse YAML file
  parser();
  // Run gtest suite
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "movebase_test");
  ros::AsyncSpinner spinner(1);
  spinner.start();
  int ret = RUN_ALL_TESTS();
  spinner.stop();
  ros::shutdown();
  return ret;
}
