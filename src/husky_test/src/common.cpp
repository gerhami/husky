#include "common.h"

TEST_F(CommonSpawnTest, testPublish) {
  ros::Duration(3.0).sleep();
  sensor_msgs::Imu status = getLastStatus();
  EXPECT_LT(fabs(status.linear_acceleration.x), 1.0); // the Husky is not moving
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "common_spawn_test");
  ros::AsyncSpinner spinner(1);
  spinner.start();
  int ret = RUN_ALL_TESTS();
  spinner.stop();
  ros::shutdown();
  return ret;
}
