#!/usr/bin/env python

import unittest, time, sys
import rospy, rostest
from nav_msgs.msg import Odometry
import yaml

PKG = 'husky_test'
NAME = 'WaypointsTest'

class WaypointsTest(unittest.TestCase):

    def yaml_stream(self, val):
        try:
            return yaml.safe_load_all(val)
        except yaml.YAMLError as exc:
            return exc

    def parseWaypoints(self, file):
        self.total_waypoints = 0
        self.x_poses = []
        self.y_poses = []
        with open(file,'r') as stream:
            poses = self.yaml_stream(stream)
            for value in poses:
                self.total_waypoints += 1 # returns the number of waypoints in the config file
                self.x_poses.append(value['pose']['pose']['position']['x']) # stores all the x positions of the waypoints
                self.y_poses.append(value['pose']['pose']['position']['y']) # stores all the y positions of the waypoints

    def pose_callback(self, data):
        current_x = data.pose.pose.position.x
        current_y = data.pose.pose.position.y
        if (self.waypoint_index < self.total_waypoints) and \
           ((self.x_poses[self.waypoint_index] - self.goal_tolerance) <= current_x <= (self.x_poses[self.waypoint_index] + self.goal_tolerance)) and \
           ((self.y_poses[self.waypoint_index] - self.goal_tolerance) <= current_y <= (self.y_poses[self.waypoint_index] + self.goal_tolerance)):
            self.waypoint_index += 1
            self.goal_reached_count += 1
            rospy.loginfo('Reached goal number ' + str(self.goal_reached_count))

    def setUp(self):
        pose_file = rospy.get_param('~pose_file')
        self.goal_tolerance = rospy.get_param('~goal_tolerance')
        self.wait_time = rospy.get_param('~wait_time')
        self.goal_reached_count = 0
        self.waypoint_index = 0
        rospy.loginfo('Pose tolerance is: ' + str(self.goal_tolerance))
        rospy.loginfo('Parsing config file...')
        self.parseWaypoints(pose_file)
        rospy.loginfo('Config file successfully parsed. ' + str(self.total_waypoints) + ' waypoints found.')
        rospy.Subscriber("/ground_truth/state", Odometry, self.pose_callback) # subscribe to pose and confirm that each point matches the destination within a tolerance

    def test_WaypointsTest(self):
        self.setUp()
        rospy.loginfo('Successfully subscribed to pose')
        rospy.sleep(self.wait_time)
        rospy.loginfo('Waited for ' + str(self.wait_time))
        rospy.loginfo('Reached: ' + str(self.goal_reached_count) + ' goals, Total: ' + str(self.total_waypoints) + ' goals')
        self.assertEqual(self.goal_reached_count, self.total_waypoints, "Goals reached not equal to goals given.")

if __name__ == '__main__':
    rospy.init_node('WaypointsTest_Node', log_level=rospy.DEBUG)
    rostest.rosrun(PKG, NAME, WaypointsTest, sys.argv)
