
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>

class CommonSpawnTest : public ::testing::Test {
private:
  bool received_first_status;
  ros::NodeHandle nh;
  ros::Subscriber imu_status_sub;
  sensor_msgs::Imu last_status;
public:
  CommonSpawnTest() : imu_status_sub(nh.subscribe("/imu/data", 1, &CommonSpawnTest::statusCallback, this)) {}

  ~CommonSpawnTest() {
    imu_status_sub.shutdown();
  }

  void statusCallback(const sensor_msgs::Imu& status) {
    ROS_INFO_STREAM("Callback received");
    last_status = status;
    received_first_status = true;
  }

  void SetUp() {
    waitForController();
    ros::Duration(0.25).sleep();
    ROS_INFO("Activating balance controller.");
    ros::Duration(0.25).sleep();
  }

  void waitForController() const {
    while(!isControllerAlive() && ros::ok()) {
      ROS_DEBUG_STREAM_THROTTLE(0.5, "Waiting for controller.");
      ros::Duration(0.1).sleep();
    }
    if (!ros::ok())
      FAIL() << "Something went wrong while executing test.";
  }

  bool isControllerAlive() const {
    return (imu_status_sub.getNumPublishers() > 0);
  }

  bool hasReceivedFirstStatus() const {
    return received_first_status;
  }

  sensor_msgs::Imu getLastStatus() {
    return last_status;
  }

  void waitForStatusMsgs() const {
    while(!hasReceivedFirstStatus() && ros::ok()) {
      ROS_DEBUG_STREAM_THROTTLE(0.5, "Waiting for status messages to be published.");
      ros::Duration(0.01).sleep();
    }
    if (!ros::ok()) {
      FAIL() << "Something went wrong while executing test.";
    }
  }
};
