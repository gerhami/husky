#include <gtest/gtest.h>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>

const double POSE_TOLERANCE = 0.5; // 0.5 m precision

class MovebaseTest : public ::testing::Test {
private:
  bool received_first_status;
  ros::NodeHandle nh;
  ros::Publisher goal_pub;
  ros::Subscriber pose_sub;
  nav_msgs::Odometry last_status;
public:
  MovebaseTest() :
    goal_pub(nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1)),
    pose_sub(nh.subscribe("/ground_truth/state", 1, &MovebaseTest::statusCallback, this)) {}

  ~MovebaseTest() {
    pose_sub.shutdown();
  }

  void statusCallback(const nav_msgs::Odometry& status) {
    ROS_INFO_STREAM("Callback received");
    last_status = status;
    received_first_status = true;
  }

  void SetUp() {
    waitForController();
    ros::Duration(0.25).sleep();
    ROS_INFO("Activating pose controller.");
    ros::Duration(0.25).sleep();
  }

  void publish_goal(geometry_msgs::PoseStamped goal) {
    goal_pub.publish(goal);
  }

  void waitForController() const {
    while(!isControllerAlive() && ros::ok()) {
      ROS_DEBUG_STREAM_THROTTLE(0.5, "Waiting for controller.");
      ros::Duration(0.1).sleep();
    }
    if (!ros::ok())
      FAIL() << "Something went wrong while executing test.";
  }

  bool isControllerAlive() const {
    return (pose_sub.getNumPublishers() > 0);
  }

  bool hasReceivedFirstStatus() const {
    return received_first_status;
  }

  nav_msgs::Odometry getLastStatus() {
    return last_status;
  }

  void waitForStatusMsgs() const {
    while(!hasReceivedFirstStatus() && ros::ok()) {
      ROS_DEBUG_STREAM_THROTTLE(0.5, "Waiting for status messages to be published.");
      ros::Duration(0.01).sleep();
    }
    if (!ros::ok()) {
      FAIL() << "Something went wrong while executing test.";
    }
  }
};
