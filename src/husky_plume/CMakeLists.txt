cmake_minimum_required(VERSION 2.8.3)
project(husky_plume)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  gazebo_plugins
  gazebo_ros
)

find_package(Boost REQUIRED COMPONENTS system)
find_package(gazebo REQUIRED)

catkin_package()

install(DIRECTORY launch
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})

install(PROGRAMS
          scripts/start_plume
        DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS} ${roscpp_INCLUDE_DIRS} ${std_msgs_INCLUDE_DIRS} src)
link_directories(${GAZEBO_LIBRARY_DIRS})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  ${GAZEBO_CXX_FLAGS} -std=c++11")

add_library(plume_pub SHARED src/plume_pub.cpp)
target_link_libraries(plume_pub ${Boost_LIBRARIES} ${GAZEBO_LIBRARIES} ${roscpp_LIBRARIES})

install(
  TARGETS plume_pub
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION},
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION},
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
