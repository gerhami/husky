#!/usr/bin/env python

import roslib; roslib.load_manifest('husky_ur5_moveit_config')

import sys
import rospy
import moveit_commander
import geometry_msgs.msg

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_interface', anonymous=True)
robot = moveit_commander.RobotCommander()
arm_group = moveit_commander.MoveGroupCommander('ur5_arm')

# arm_group.set_named_target('stow')
# plan1 = arm_group.go()
#
# rospy.sleep(5)

arm_group.set_named_target('hit_neutral')
plan1 = arm_group.go()

rospy.sleep(3)

arm_group.set_named_target('hit_left')
plan1 = arm_group.go()

rospy.sleep(3)

arm_group.set_named_target('hit_right')
plan1 = arm_group.go()

rospy.sleep(3)

moveit_commander.roscpp_shutdown()
