#!/usr/bin/env python

""" This is a task to let the husky explore a space. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

import smach
from smach import StateMachine
from smach_ros import SimpleActionState, MonitorState

from frontier_exploration.msg import ExploreTaskAction, ExploreTaskGoal
from geometry_msgs.msg import Point
from std_srvs.srv import Empty

def frontier_exploration(map_length, map_width, length_div, width_div):

    x_off = map_width/width_div
    y_off = map_length/length_div

    x = -map_length/2
    y = -map_width/2

    for i in range(0, width_div):
        for j in range(0, length_div):

            sm1 = StateMachine(outcomes=['succeeded','aborted','preempted'])

            with sm1:

                poly_goal = ExploreTaskGoal()
                time = rospy.Time.now()
                frame = '/map'
                poly_goal.explore_center.header.frame_id = frame
                poly_goal.explore_center.header.stamp = time
                poly_goal.explore_center.point = Point(x=x+x_off/2, y=y+y_off/2, z=0)
                poly_goal.explore_boundary.header.frame_id = frame
                poly_goal.explore_boundary.header.stamp = time
                poly_goal.explore_boundary.polygon.points = [Point(x=x, y=y, z=0),
                                                      Point(x=x+x_off, y=y, z=0),
                                                      Point(x=x+x_off, y=y+y_off, z=0),
                                                      Point(x=x, y=y+y_off, z=0)]

                StateMachine.add('GET_POLYGON', SimpleActionState('explore_server',
                                 ExploreTaskAction,
                                 goal = poly_goal),
                                 transitions={'succeeded':'GOAL_REACHED', 'aborted':'GOAL_REACHED'})


                def monitor_cb(ud, msg):
                    return False

                StateMachine.add('GOAL_REACHED', MonitorState('move_base/result', Empty, monitor_cb),
                                 transitions={'invalid':'succeeded', 'valid':'GOAL_REACHED', 'preempted':'preempted'})

            return sm1

            x += off_x

        y += y_off
        x = -map_length/2

def main():
    rospy.init_node('smach')

    rospy.wait_for_service('move_base/make_plan') # waits for move_base to be active
    rospy.wait_for_service('explore_server/get_loggers') # waits for the exploration server to be active

    # add try and exceptions
    map_length = rospy.get_param('~map_length', 20)
    map_width = rospy.get_param('~map_width', 20)
    length_div = rospy.get_param('~length_div', 4)
    width_div = rospy.get_param('~width_div', 4)

    sm1_executor = frontier_exploration(map_length, map_width, length_div, width_div)
    outcome = sm1_executor.execute()

    rospy.spin()

if __name__ == '__main__':
    main()
