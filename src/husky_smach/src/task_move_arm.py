#!/usr/bin/env python

import roslib; roslib.load_manifest('husky_smach')

import sys
import rospy
import yaml

from math import pi

import smach
from smach import Iterator, StateMachine, CBState
from smach_ros import IntrospectionServer, ServiceState

import move_base as move_base
import cv as cv

import moveit_commander

from std_srvs.srv import Empty
import geometry_msgs.msg

class MoveArm(smach.State):
    def __init__(self, position):
        smach.State.__init__(self, outcomes=['succeeded', 'aborted'])
        moveit_commander.roscpp_initialize(sys.argv)
        robot = moveit_commander.RobotCommander()
        self.arm_group = moveit_commander.MoveGroupCommander('ur5_arm')
        self.position = position

    def execute(self, userdata):
        self.arm_group.set_named_target(self.position)
        try:
            plan1 = self.arm_group.go()
            rospy.loginfo('Executing arm move')
            return 'succeeded'
        except:
            return 'aborted'

class MoveGripper(smach.State):
    def __init__(self, position):
        smach.State.__init__(self, outcomes=['succeeded', 'aborted'])
        moveit_commander.roscpp_initialize(sys.argv)
        robot = moveit_commander.RobotCommander()
        self.arm_group = moveit_commander.MoveGroupCommander('gripper')
        self.position = position

    def execute(self, userdata):
        self.arm_group.set_named_target(self.position)
        try:
            plan1 = self.arm_group.go()
            rospy.loginfo('Executing gripper move')
            return 'succeeded'
        except:
            return 'aborted'

def move_arm():

    sm1 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    sm1.userdata.x = 1
    sm1.userdata.y = 0
    sm1.userdata.yaw = 0

    with sm1:

        StateMachine.add('MOVE_ARM_GRAB', MoveArm('grab'),
                         {'succeeded':'MOVE_TO_CAN',
                          'aborted':'aborted'})

        StateMachine.add('MOVE_TO_CAN', move_base.MoveBaseState('/map'),
                         {'succeeded':'CLOSE_GRIPPER',
                          'aborted':'aborted'})

        StateMachine.add('CLOSE_GRIPPER', MoveGripper('semi_closed'),
                         {'succeeded':'GET_NEW_GOAL',
                          'aborted':'aborted'})

        @smach.cb_interface(input_keys=['x', 'yaw'],
                            output_keys=['x', 'yaw'],
                            outcomes=['succeeded'])

        def get_current_goal(ud):
            ud.x = -1
            ud.yaw = pi/2
            return 'succeeded'

        StateMachine.add('GET_NEW_GOAL', CBState(get_current_goal),
                         {'succeeded':'MOVE_BACK'})

        StateMachine.add('MOVE_BACK', move_base.MoveBaseState('/map'),
                         {'succeeded':'succeeded',
                          'aborted':'aborted'})
    return sm1

def main():
    rospy.init_node('smach')
    # rospy.wait_for_service('move_base/make_plan')
    sm1_executor = move_arm()
    intro_server = IntrospectionServer('move_arm',sm1_executor,'/MOVE_ARM')
    intro_server.start()
    outcome = sm1_executor.execute()
    rospy.spin()

if __name__ == '__main__':
    main()
