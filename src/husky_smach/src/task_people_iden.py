#!/usr/bin/env python

""" This is a task to let the husky move and identify a person. """

import roslib; roslib.load_manifest('husky_smach')
import rospy
import yaml

import smach
from smach import Iterator, StateMachine, CBState
from smach_ros import IntrospectionServer, ServiceState

import move_base as move_base
import cv as cv

from std_srvs.srv import Empty

LOOKAROUND_SLEEP_DURATION = 2

def get_goals(file):

    sm0 = StateMachine(outcomes=['succeeded','aborted','preempted'])
    sm0.userdata.x = []
    sm0.userdata.y = []
    sm0.userdata.yaw = []
    sm0.userdata.no_of_goals = 0

    with sm0:
        StateMachine.add('READ_CONFIG_FILE', move_base.ParseGoalStates(file),
                         transitions={'succeeded':'DISPLAY_READ_GOALS', 'aborted':'aborted'})

        StateMachine.add('DISPLAY_READ_GOALS', move_base.DisplayGoals(),
                         transitions={'succeeded':'succeeded'})

    outcome0 = sm0.execute()

    return (sm0.userdata.x, sm0.userdata.y, sm0.userdata.yaw, sm0.userdata.no_of_goals)

def move_around_smach(x, y, yaw, no_of_goals):

    sm1 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    sm1.userdata.x = x
    sm1.userdata.y = y
    sm1.userdata.yaw = yaw
    sm1.userdata.no_of_goals = no_of_goals

    sm1.userdata.current_x = 0
    sm1.userdata.current_y = 0
    sm1.userdata.current_yaw = 0

    sm1.userdata.people_identified = 0

    with sm1:

        sm_it = Iterator(outcomes = ['succeeded','preempted','aborted'],
                         input_keys = ['x', 'y', 'yaw', 'current_x', 'current_y', 'current_yaw', 'people_identified'],
                         output_keys = ['current_x', 'current_y', 'current_yaw', 'people_identified'],
                         it = range(0, sm1.userdata.no_of_goals, 1),
                         it_label = 'goal_index',
                         exhausted_outcome = 'succeeded')

        with sm_it:

            container_sm = StateMachine(outcomes = ['succeeded','preempted','aborted','continue'],
                                        input_keys = ['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw', 'people_identified'],
                                        output_keys = ['current_x', 'current_y', 'current_yaw', 'people_identified'])

            with container_sm:

                @smach.cb_interface(input_keys=['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw'],
                                    output_keys=['current_x', 'current_y', 'current_yaw'],
                                    outcomes=['succeeded'])

                def get_current_goal(ud):
                    ud.current_x = ud.x[ud.goal_index]
                    ud.current_y = ud.y[ud.goal_index]
                    ud.current_yaw = ud.yaw[ud.goal_index]
                    rospy.loginfo('Current Goal (x, y, yaw) = (%f, %f, %f)'%(ud.current_x, ud.current_y, ud.current_yaw))
                    return 'succeeded'

                StateMachine.add('GET_CURRENT_GOAL', CBState(get_current_goal),
                                 {'succeeded':'MOVE_TO_GOAL'})

                StateMachine.add('MOVE_TO_GOAL', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'IDENTIFY_PERSON'},
                                 remapping={'x':'current_x',
                                            'y':'current_y',
                                            'yaw':'current_yaw'})

                StateMachine.add('IDENTIFY_PERSON', cv.Identify(),
                                 transitions={'succeeded':'succeeded', 'aborted':'aborted'})

                # StateMachine.add('TAKE_PICTURE', ServiceState('/image_saver/save', Empty),
                #                  transitions={'succeeded':'continue'})

            Iterator.set_contained_state('CONTAINER_STATE',
                                         container_sm,
                                         loop_outcomes=['continue'])

        StateMachine.add('GOAL_ITERATOR', sm_it,
                     {'succeeded':'succeeded',
                      'aborted':'aborted'})

    return sm1

def main():
    rospy.init_node('smach')
    rospy.wait_for_service('move_base/make_plan')

    pose_file = rospy.get_param('~pose_file')
    (x, y, yaw, no_of_goals) = get_goals(pose_file)
    sm1_executor = move_around_smach(x, y, yaw, no_of_goals)

    intro_server = IntrospectionServer('multiple_goals',sm1_executor,'/MOVE_BASE_MULTIPLE_GOALS')
    intro_server.start()

    outcome = sm1_executor.execute()

    rospy.spin()
    intro_server.stop()

if __name__ == '__main__':
    main()
