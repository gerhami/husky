#!/usr/bin/env python

""" This is a task to let the husky explore a space. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

import tf
from math import pi

from scipy.spatial import Voronoi
import random

import visualization_msgs
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Point, Quaternion, Vector3
from std_msgs.msg import ColorRGBA, Float64

import smach
from smach import Iterator, CBState, StateMachine, Concurrence, State
import move_base as move_base

ANGLES = [0, pi/4, pi/2, pi, -pi/4, -pi/2]

def get_voronoi(map_length, map_width, iterations):

    x_points = []
    y_points = []
    yaw_points = []

    for rep in range(iterations):
        N = 20 # hardcoded for now
        x = [random.randrange(-map_length/2, map_length/2) for i in range(N)]
        y = [random.randrange(-map_width/2, map_width/2) for i in range(N)]
        points = list(zip(x, y))

        vor = Voronoi(points, qhull_options='Qbb Qc Qx')

        for i, reg in enumerate(vor.regions):
            x_points.append(points[i][0])
            y_points.append(points[i][1])

    yaw_points = [random.choice(ANGLES) for i in range(len(x_points))]
    return (x_points, y_points, yaw_points)

def show_markers(goals):

    array = MarkerArray()
    pub = rospy.Publisher('/visualization_marker_array', MarkerArray, queue_size=10)
    (x_points, y_points, yaw_points) = zip(*goals)

    for i in range(0, len(x_points)):
        mk = Marker()
        mk.header.frame_id = '/map'
        mk.header.stamp = rospy.Time()
        mk.ns = 'goals'
        mk.id = i
        mk.type = 2
        mk.action = 0
        quat = tf.transformations.quaternion_from_euler(0.0, 0.0, yaw_points[i])
        mk.pose.orientation = Quaternion(*quat)
        mk.pose.position = Point(x_points[i], y_points[i], 0.0)
        mk.scale = Vector3(0.1, 0.1, 0.1)
        mk.color = ColorRGBA(0.0, 1.0, 0.0, 1.0)
        array.markers.append(mk)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        pub.publish(array)
        rate.sleep()

def frontier_exploration(goals):

    sm1 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    (sm1.userdata.x, sm1.userdata.y, sm1.userdata.yaw) = zip(*goals)

    sm1.userdata.current_x = 0
    sm1.userdata.current_y = 0
    sm1.userdata.current_yaw = 0

    with sm1:

        sm_it = Iterator(outcomes = ['succeeded','preempted','aborted'],
                         input_keys = ['x', 'y', 'yaw', 'current_x', 'current_y', 'current_yaw'],
                         output_keys = ['current_x', 'current_y', 'current_yaw'],
                         it = range(0, len(sm1.userdata.x), 1),
                         it_label = 'goal_index',
                         exhausted_outcome = 'succeeded')

        with sm_it:

            container_sm = StateMachine(outcomes = ['succeeded','preempted','aborted','continue'],
                                        input_keys = ['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw'],
                                        output_keys = ['current_x', 'current_y', 'current_yaw'])

            with container_sm:

                @smach.cb_interface(input_keys=['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw'],
                                    output_keys=['current_x', 'current_y', 'current_yaw'],
                                    outcomes=['succeeded'])

                def get_current_goal(ud):
                    ud.current_x = ud.x[ud.goal_index]
                    ud.current_y = ud.y[ud.goal_index]
                    ud.current_yaw = ud.yaw[ud.goal_index]
                    rospy.loginfo('Current Goal (x, y, yaw) = (%f, %f, %f)'%(ud.current_x, ud.current_y, ud.current_yaw))
                    return 'succeeded'

                StateMachine.add('GET_CURRENT_GOAL', CBState(get_current_goal),
                                 {'succeeded':'MOVE_TO_GOAL'})

                StateMachine.add('MOVE_TO_GOAL', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'continue'},
                                 remapping={'x':'current_x',
                                            'y':'current_y',
                                            'yaw':'current_yaw'})

            Iterator.set_contained_state('CONTAINER_STATE',
                                         container_sm,
                                         loop_outcomes=['continue'])

        StateMachine.add('GOAL_ITERATOR', sm_it,
                     {'succeeded':'succeeded',
                      'aborted':'aborted'})

    return sm1

def main():
    rospy.init_node('smach')

    rospy.wait_for_service('move_base/make_plan') # waits for move_base to be active

    map_length = rospy.get_param('~map_length', 18)
    map_width = rospy.get_param('~map_width', 18)
    iterations = rospy.get_param('~iterations', 3)
    show = rospy.get_param('~show', False)

    (x_points, y_points, yaw_points) = get_voronoi(map_length, map_width, iterations)
    goals = zip(x_points, y_points, yaw_points)

    if show:
        show_markers(goals)
    else:
        sm0_executor = frontier_exploration(goals)
        outcome = sm0_executor.execute()

    rospy.spin()

if __name__ == '__main__':
    main()
