#!/usr/bin/env python

""" This is a task to let the husky move to a multiple random goals. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

import smach
from smach import Iterator, StateMachine, CBState
from smach_ros import IntrospectionServer

import move_base as move_base

def random_move_around_smach(no_of_goals):

    sm0 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    #(sm0.userdata.x, sm0.userdata.y, sm0.userdata.yaw) = move_base.RandomGoals(no_of_goals)
    sm0.userdata.x = []
    sm0.userdata.y = []
    sm0.userdata.yaw = []

    sm0.userdata.current_x = 0
    sm0.userdata.current_y = 0
    sm0.userdata.current_yaw = 0

    with sm0:

        StateMachine.add('GET_RANDOM_GOALS', move_base.RandomGoals(no_of_goals),
                         {'succeeded':'GOAL_ITERATOR'})

        sm_it = Iterator(outcomes = ['succeeded','preempted','aborted'],
                         input_keys = ['x', 'y', 'yaw', 'current_x', 'current_y', 'current_yaw'],
                         output_keys = ['current_x', 'current_y', 'current_yaw'],
                         it = range(0, no_of_goals, 1),
                         it_label = 'goal_index',
                         exhausted_outcome = 'succeeded')

        with sm_it:

            container_sm = StateMachine(outcomes = ['succeeded','preempted','aborted','continue'],
                                        input_keys = ['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw'],
                                        output_keys = ['current_x', 'current_y', 'current_yaw'])

            with container_sm:

                @smach.cb_interface(input_keys=['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw'],
                                    output_keys=['current_x', 'current_y', 'current_yaw'],
                                    outcomes=['succeeded'])

                def get_current_goal(ud):
                    ud.current_x = ud.x[ud.goal_index]
                    ud.current_y = ud.y[ud.goal_index]
                    ud.current_yaw = ud.yaw[ud.goal_index]
                    rospy.loginfo('Current Goal (x, y, yaw) = (%f, %f, %f)'%(ud.current_x, ud.current_y, ud.current_yaw))
                    return 'succeeded'

                StateMachine.add('GET_CURRENT_GOAL', CBState(get_current_goal),
                                 {'succeeded':'MOVE_TO_GOAL'})

                StateMachine.add('MOVE_TO_GOAL', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'continue'},
                                 remapping={'x':'current_x',
                                            'y':'current_y',
                                            'yaw':'current_yaw'})

            Iterator.set_contained_state('CONTAINER_STATE',
                                         container_sm,
                                         loop_outcomes=['continue'])

        StateMachine.add('GOAL_ITERATOR', sm_it,
                     {'succeeded':'succeeded',
                      'aborted':'aborted'})

    return sm0

def main():
    rospy.init_node('smach')
    rospy.wait_for_service('move_base/make_plan')

    no_of_goals = rospy.get_param('~no_of_goals')
    sm0_executor = random_move_around_smach(no_of_goals)

    intro_server = IntrospectionServer('random_goals',sm0_executor,'/MOVE_BASE_RANDOM_GOALS')
    intro_server.start()

    outcome = sm0_executor.execute()

    rospy.spin()
    intro_server.stop()

if __name__ == '__main__':
    main()
