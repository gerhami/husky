#!/usr/bin/env python

""" This is a task to let the husky move and identify a person. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

import yaml
import random

import smach
from smach import Iterator, StateMachine, CBState
from smach_ros import IntrospectionServer, ServiceState

import move_base as move_base
import cv as cv

from std_srvs.srv import Empty

REACH_LENGTH = 5 # m^2

def find_person():

    sm0 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    sm0.userdata.current_x = 0
    sm0.userdata.current_y = 0
    sm0.userdata.current_yaw = 0
    sm0.userdata.attempts = 0

    sm0.userdata.people_identified = 0

    with sm0:

        @smach.cb_interface(input_keys=['current_x', 'current_y', 'current_yaw', 'attempts'],
                            output_keys=['current_x', 'current_y', 'current_yaw', 'attempts'],
                            outcomes=['succeeded'])

        def get_current_goal(ud):
            ud.current_x = random.uniform(-REACH_LENGTH, REACH_LENGTH)
            ud.current_y = random.uniform(-REACH_LENGTH, REACH_LENGTH)
            ud.current_yaw = random.randint(-360, 360)
            ud.attempts += 1
            rospy.loginfo('New random goal (x, y, yaw) = (%f, %f, %f)'%(ud.current_x, ud.current_y, ud.current_yaw))
            rospy.loginfo('Goal attempt %f'%ud.attempts)
            return 'succeeded'

        StateMachine.add('GET_CURRENT_GOAL', CBState(get_current_goal),
                         {'succeeded':'MOVE_TO_GOAL'})

        StateMachine.add('MOVE_TO_GOAL', move_base.MoveBaseState('/base_link'),
                         transitions={'succeeded':'IDENTIFY_PERSON'},
                         remapping={'x':'current_x',
                                    'y':'current_y',
                                    'yaw':'current_yaw'})

        StateMachine.add('IDENTIFY_PERSON', cv.Identify(),
                         transitions={'succeeded':'succeeded', 'aborted':'GET_CURRENT_GOAL'})

    return sm0

def main():
    rospy.init_node('smach')

    rospy.wait_for_service('move_base/make_plan')

    sm0_executor = find_person()

    intro_server = IntrospectionServer('find_person',sm0_executor,'/FIND_PERSON')
    intro_server.start()

    outcome = sm0_executor.execute()

    rospy.spin()
    intro_server.stop()

if __name__ == '__main__':
    main()
