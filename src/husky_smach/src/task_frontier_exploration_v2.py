#!/usr/bin/env python

""" This is a task to let the husky explore a space. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

from math import pi

import smach
from smach import Iterator, CBState, StateMachine

import move_base as move_base

def frontier_exploration(side_length, divisions):

    sm1 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    sm1.userdata.offset = side_length/divisions
    sm1.userdata.x = sm1.userdata.offset/2
    sm1.userdata.y = sm1.userdata.offset/2
    sm1.userdata.x_neg = -sm1.userdata.x
    sm1.userdata.y_neg = -sm1.userdata.y
    sm1.userdata.zero = 0
    sm1.userdata.pi = pi
    sm1.userdata.h_pi = pi/2

    with sm1:

        sm_it = Iterator(outcomes = ['succeeded','preempted','aborted'],
                         input_keys = ['x', 'y', 'x_neg', 'y_neg', 'offset', 'zero', 'pi', 'h_pi'],
                         output_keys = ['x', 'y', 'x_neg', 'y_neg'],
                         it = range(0, divisions, 1),
                         it_label = 'divisions',
                         exhausted_outcome = 'succeeded')

        with sm_it:

            container_sm = StateMachine(outcomes = ['succeeded','preempted','aborted','continue'],
                                        input_keys = ['x', 'y', 'x_neg', 'y_neg', 'offset', 'zero', 'pi', 'h_pi'],
                                        output_keys = ['x', 'y', 'x_neg', 'y_neg'])

            with container_sm:

                StateMachine.add('GOAL_TOP', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'GOAL_RIGHT'},
                                 remapping={'x':'zero',
                                            'y':'y_neg',
                                            'yaw':'zero'})

                StateMachine.add('GOAL_RIGHT', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'GOAL_BOTTOM'},
                                 remapping={'x':'x',
                                            'y':'zero',
                                            'yaw':'h_pi'})

                StateMachine.add('GOAL_BOTTOM', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'GOAL_LEFT'},
                                 remapping={'x':'zero',
                                            'y':'y',
                                            'yaw':'pi'})

                StateMachine.add('GOAL_LEFT', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'OFFSET_GOALS'},
                                 remapping={'x':'x_neg',
                                            'y':'zero',
                                            'yaw':'h_pi'})

                @smach.cb_interface(input_keys=['x', 'y', 'x_neg', 'y_neg', 'offset'],
                                    output_keys=['x', 'y', 'x_neg', 'y_neg', 'offset'],
                                    outcomes=['succeeded'])

                def get_new_goal(ud):
                    ud.x += ud.offset
                    ud.y += ud.offset
                    ud.x_neg = -ud.x
                    ud.y_neg = -ud.y
                    return 'succeeded'

                StateMachine.add('OFFSET_GOALS', CBState(get_new_goal),
                                 {'succeeded':'continue'})

            Iterator.set_contained_state('CONTAINER_STATE',
                                         container_sm,
                                         loop_outcomes=['continue'])

        StateMachine.add('GOAL_ITERATOR', sm_it,
                     {'succeeded':'succeeded',
                      'aborted':'aborted'})

    return sm1

def main():
    rospy.init_node('smach')

    rospy.wait_for_service('move_base/make_plan') # waits for move_base to be active

    # add try and exceptions
    side_length = rospy.get_param('~side_length', 10)
    divisions = rospy.get_param('~divisions', 2)

    if side_length % divisions != 0:
        rospy.logwarn('Side_length is not divisable by divisions')

    sm1_executor = frontier_exploration(side_length, divisions)
    outcome = sm1_executor.execute()

    rospy.spin()

if __name__ == '__main__':
    main()
