#!/usr/bin/env python

""" This file generates easy to use smach states needed for computer vision. """

import roslib; roslib.load_manifest('husky_smach')

import rospy

import smach
from smach import State

from opencv_apps.msg import RectArrayStamped
from apriltag_ros.msg import AprilTagDetection
from apriltag_ros.msg import AprilTagDetectionArray
from geometry_msgs.msg import PoseWithCovarianceStamped
from std_msgs.msg import Float64

class Identify(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['succeeded', 'aborted'], input_keys=['people_identified'], output_keys=['people_identified'])

    def execute(self, userdata):
        msg = rospy.wait_for_message("people_detect/found", RectArrayStamped)
        try:
            if msg.rects[0].x > 0:
                rospy.loginfo('Found person')
                userdata.people_identified += 1
                return 'succeeded'
        except:
            rospy.loginfo('Person not found')
            return 'aborted'

class TagIdentify(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['succeeded', 'aborted'], input_keys=['tag_id'], output_keys=['tag_id'])

    def execute(self, userdata):
        msg = rospy.wait_for_message('tag_detections', AprilTagDetectionArray)
        try:
            if msg.detections:
                userdata.tag_id = msg.detections[0].id[0]
                rospy.loginfo('Starting tag ID is: %f'%userdata.tag_id)
                return 'succeeded'
        except:
            rospy.loginfo('Tag ID failed')
            return 'aborted'
