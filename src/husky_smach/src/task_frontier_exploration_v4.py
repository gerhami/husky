#!/usr/bin/env python

""" This is a task to let the husky explore a space. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

from husky_msgs.msg import VoronoiArray

import smach
from smach import Iterator, CBState, StateMachine, Concurrence, State
from smach_ros import IntrospectionServer
import move_base as move_base

def frontier_exploration(goals, iterations, no_of_regions, pts_per_region):

    sm1 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    (sm1.userdata.x, sm1.userdata.y, sm1.userdata.yaw) = zip(*goals)

    sm1.userdata.current_x = 0
    sm1.userdata.current_y = 0
    sm1.userdata.current_yaw = 0

    sm1.userdata.current_iteration = 0
    sm1.userdata.current_region = 0

    with sm1:

        sm_it = Iterator(outcomes = ['succeeded','preempted','aborted'],
                         input_keys = ['x', 'y', 'yaw', 'current_x', 'current_y', 'current_yaw', 'current_region', 'current_iteration'],
                         output_keys = ['current_x', 'current_y', 'current_yaw', 'current_region', 'current_iteration'],
                         it = range(0, len(sm1.userdata.x), 1),
                         it_label = 'goal_index',
                         exhausted_outcome = 'succeeded')

        with sm_it:

            container_sm = StateMachine(outcomes = ['succeeded','preempted','aborted','continue'],
                                        input_keys = ['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw', 'current_region', 'current_iteration'],
                                        output_keys = ['current_x', 'current_y', 'current_yaw', 'current_region', 'current_iteration'])

            with container_sm:

                @smach.cb_interface(input_keys=['x', 'y', 'yaw', 'goal_index', 'current_x', 'current_y', 'current_yaw', 'current_region', 'current_iteration'],
                                    output_keys=['current_x', 'current_y', 'current_yaw', 'current_region', 'current_iteration'],
                                    outcomes=['succeeded'])

                def get_current_goal(ud):
                    if ud.goal_index % (no_of_regions*pts_per_region) == 0:
                        rospy.loginfo('Starting iteration [%s] of [%s]'%(ud.current_iteration + 1, iterations))
                        ud.current_region = 0
                        ud.current_iteration += 1
                    if ud.goal_index % pts_per_region == 0:
                        rospy.loginfo('Moving to Voronoi region [%s] of [%s]'%(ud.current_region + 1, no_of_regions))
                        ud.current_region += 1
                    ud.current_x = ud.x[ud.goal_index]
                    ud.current_y = ud.y[ud.goal_index]
                    ud.current_yaw = ud.yaw[ud.goal_index]
                    rospy.loginfo('Current Goal (x, y, yaw) = (%f, %f, %f)'%(ud.current_x, ud.current_y, ud.current_yaw))
                    return 'succeeded'

                StateMachine.add('GET_CURRENT_GOAL', CBState(get_current_goal),
                                 {'succeeded':'MOVE_TO_GOAL'})

                StateMachine.add('MOVE_TO_GOAL', move_base.MoveBaseState('/map'),
                                 transitions={'succeeded':'continue'},
                                 remapping={'x':'current_x',
                                            'y':'current_y',
                                            'yaw':'current_yaw'})

            Iterator.set_contained_state('CONTAINER_STATE',
                                         container_sm,
                                         loop_outcomes=['continue'])

        StateMachine.add('GOAL_ITERATOR', sm_it,
                     {'succeeded':'succeeded',
                      'aborted':'aborted'})

    return sm1

def callback(msg):
    if msg.x:
        goals = zip(msg.x, msg.y, msg.yaw)
        sm0_executor = frontier_exploration(goals, msg.iterations, msg.no_of_regions, msg.pts_per_region)
        intro_server = IntrospectionServer('frontier_exploration',sm0_executor,'/FRONTIER_EXPLORATION')
        intro_server.start()
        outcome = sm0_executor.execute()

def main():
    rospy.init_node('smach')
    rospy.wait_for_service('move_base/make_plan') # waits for move_base to be active
    rospy.Subscriber('voronoi_points', VoronoiArray, callback)
    rospy.spin()

if __name__ == '__main__':
    main()
