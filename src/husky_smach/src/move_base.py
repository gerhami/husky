#!/usr/bin/env python

""" This file generates easy to use smach states needed to move the robot base. """

import roslib; roslib.load_manifest('husky_smach')

import rospy
import tf

import math
import random
import yaml

import smach
from smach import State, Sequence
from smach_ros import SimpleActionState, ServiceState

from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction
from geometry_msgs.msg import Pose, PoseStamped, Point, Quaternion
from nav_msgs.srv import GetPlan, GetPlanRequest

HALF_MAP_SIZE = 10 # m^2

class MoveBaseState(SimpleActionState):
    """Calls a move_base action server with the goal (x, y, yaw) from userdata"""
    def __init__(self, frame='/map'):
        SimpleActionState.__init__(self, 'move_base', MoveBaseAction, input_keys=['x', 'y', 'yaw'], outcomes=['succeeded'], goal_cb=self.__goal_cb)
        self.frame = frame

    def __goal_cb(self, userdata, old_goal):
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = self.frame
        goal.target_pose.header.stamp = rospy.Time.now()

        quat = tf.transformations.quaternion_from_euler(0, 0, userdata.yaw)
        goal.target_pose.pose.orientation = Quaternion(*quat)
        goal.target_pose.pose.position = Point(userdata.x, userdata.y, 0)
        return goal

class ParseGoalStates(smach.State):
    """
    Reads a config file for multiple goals
    """
    def __init__(self, file):
        smach.State.__init__(self, outcomes=['succeeded', 'aborted'], input_keys=['x', 'y', 'yaw', 'no_of_goals'], output_keys=['x', 'y', 'yaw', 'no_of_goals'])
        self.file = file

    def execute(self, userdata):
        rospy.loginfo('Reading files')
        with open(self.file,'r') as stream:
            try:
                poses = yaml.safe_load_all(stream)
                for value in poses:
                    userdata.x.append(value['x'])
                    userdata.y.append(value['y'])
                    userdata.yaw.append(value['yaw'])
                    userdata.no_of_goals += 1
                return 'succeeded'
            except yaml.YAMLError as exc:
                return 'aborted'

class DisplayGoals(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['succeeded'], input_keys=['x', 'y', 'yaw', 'no_of_goals'])

    def execute(self, userdata):
        rospy.loginfo('Executing state DISPLAY')
        rospy.loginfo('Number of Goals = %f'%userdata.no_of_goals)
        rospy.loginfo('x poses: {}'.format(userdata.x))
        rospy.loginfo('y poses: {}'.format(userdata.y))
        rospy.loginfo('yaw poses: {}'.format(userdata.yaw))
        return 'succeeded'

class RandomGoals(smach.State):
    def __init__(self, no_of_goals):
        smach.State.__init__(self, outcomes=['succeeded'], input_keys=['x', 'y', 'yaw'], output_keys=['x', 'y', 'yaw'])
        self.no_of_goals = no_of_goals

    def execute(self, userdata):
        rospy.loginfo('Generating %f random goals'%self.no_of_goals)
        angles = [0, 90, 180, 270]
        for i in range(self.no_of_goals):
            userdata.x.append(random.uniform(0, HALF_MAP_SIZE - 1))
            userdata.y.append(random.uniform(0, HALF_MAP_SIZE - 1))
            userdata.yaw.append(random.choice(angles))
        rospy.loginfo('Yielded:')
        rospy.loginfo('x poses: {}'.format(userdata.x))
        rospy.loginfo('y poses: {}'.format(userdata.y))
        rospy.loginfo('yaw poses: {}'.format(userdata.yaw))
        return 'succeeded'

class TagToStartingPose(smach.State):
    def __init__(self, file):
        smach.State.__init__(self, outcomes=['succeeded', 'aborted'],
                                   input_keys=['starting_x', 'starting_y', 'starting_yaw', 'tag_id'],
                                   output_keys=['starting_x', 'starting_y', 'starting_yaw', 'tag_id'])
        self.file = file

    def execute(self, userdata):
        with open(self.file,'r') as stream:
            try:
                starting_locs = yaml.safe_load_all(stream)
                for loc in starting_locs:
                    if userdata.tag_id == loc['id']:
                        userdata.starting_x = loc['x']
                        userdata.starting_y = loc['y']
                        userdata.starting_yaw = loc['yaw']
                        rospy.loginfo('Current starting position is (%f, %f, %f)'%(userdata.starting_x, userdata.starting_y, userdata.starting_yaw))
                        return 'succeeded'

            except yaml.YAMLError as exc:
                rospy.loginfo('Error parsing starting position: %s'%exc)
                return 'aborted'

class FindClosestCharger(smach.State):
    def __init__(self, file):
        smach.State.__init__(self, outcomes=['succeeded', 'aborted'],
                                   input_keys=['starting_x', 'starting_y', 'starting_yaw', 'charger_x', 'charger_y', 'charger_yaw'],
                                   output_keys=['starting_x', 'starting_y', 'starting_yaw', 'charger_x', 'charger_y', 'charger_yaw'])
        self.file = file

    def get_distance(self, x1, y1, x2, y2):
        return math.sqrt(math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2))

    def execute(self, userdata):
        distance = 9999999
        with open(self.file,'r') as stream:
            try:
                charger_locs = yaml.safe_load_all(stream)
                for loc in charger_locs:
                    if self.get_distance(userdata.starting_x, userdata.starting_y, loc['x'], loc['y']) < distance:
                        userdata.charger_x = loc['x']
                        userdata.charger_y = loc['y']
                        userdata.charger_yaw = loc['yaw']

                rospy.loginfo('The closest charger is at (%f, %f, %f)'%(userdata.charger_x, userdata.charger_y, userdata.charger_yaw))
                return 'succeeded'

            except yaml.YAMLError as exc:
                rospy.loginfo('Error finding closest charger position: %s'%exc)
                return 'aborted'
