#!/usr/bin/env python

""" This is a task to let the husky find a plume source. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

import random
from math import pi

import smach
from smach import StateMachine, CBState
from smach_ros import IntrospectionServer

from uuv_sensor_ros_plugins_msgs.msg import ChemicalParticleConcentration

import move_base as move_base

CC_TOLERANCE = 0.1 # allowable difference in concentration between two points
GOAL_INCREMENT = 2 # meters
DIRECTIONS = ['STRAIGHT', 'BACK', 'LEFT', 'RIGHT']

MAX_CONCENTRATION = 2.0

def find_plume_source():

    sm0 = StateMachine(outcomes=['succeeded', 'aborted', 'preempted'])

    sm0.userdata.x = 0
    sm0.userdata.y = 0
    sm0.userdata.yaw = 0

    sm0.userdata.concentration = 0
    sm0.userdata.prev_concentration = 0
    sm0.userdata.prev_move = 'LEFT'

    with sm0:

        @smach.cb_interface(input_keys=['concentration', 'prev_concentration'],
                            output_keys=['concentration', 'prev_concentration'],
                            outcomes=['succeeded', 'aborted'])

        def get_concentration(ud):
            try:
                ud.prev_concentration = ud.concentration
                msg = rospy.wait_for_message('/particle_concentration', ChemicalParticleConcentration)
                ud.concentration = msg.concentration
                rospy.loginfo('Current concentration is %f'%ud.concentration)
                return 'succeeded'
            except Exception as e:
                rospy.loginfo('Error getting new concentration: %s'%e)
                return 'aborted'

        StateMachine.add('GET_CONCENTRATION', CBState(get_concentration),
                         {'succeeded':'CHECK_CONCENTRATION', 'aborted':'aborted'})

        @smach.cb_interface(input_keys=['concentration', 'prev_concentration'],
                            output_keys=['concentration', 'prev_concentration'],
                            outcomes=['higher', 'lower', 'same'])

        def read_concentration(ud):
            outcome = ''
            if ud.concentration > ud.prev_concentration + CC_TOLERANCE:
                rospy.loginfo('Current concentration is higher than previous location - moving in same direction')
                outcome = 'higher'
            elif ud.concentration < ud.prev_concentration - CC_TOLERANCE:
                rospy.loginfo('Current concentration is lower than previous location - returning to previous direction')
                outcome = 'lower'
            elif ud.prev_concentration - CC_TOLERANCE <= ud.concentration <= ud.prev_concentration + CC_TOLERANCE:
                rospy.loginfo('Current concentration is the same as the previous location - moving in new direction')
                outcome = 'same'
            return outcome

        StateMachine.add('CHECK_CONCENTRATION', CBState(read_concentration),
                         {'higher':'GET_GOAL_HIGH', 'lower':'GET_GOAL_LOW', 'same':'GET_GOAL_SAME'})

        @smach.cb_interface(input_keys=['x', 'y', 'yaw', 'prev_move'],
                            output_keys=['x', 'y', 'yaw', 'prev_move'],
                            outcomes=['succeeded'])

        def get_goal(ud, outcome):
            if outcome == 'HIGH': # Move in the same direction
                if ud.prev_move == 'STRAIGHT':
                    ud.x += GOAL_INCREMENT
                    ud.yaw = 0
                elif ud.prev_move == 'BACK':
                    ud.x -= GOAL_INCREMENT
                    ud.yaw = pi
                elif ud.prev_move == 'LEFT':
                    ud.y += GOAL_INCREMENT
                    ud.yaw = pi/2
                elif ud.prev_move == 'RIGHT':
                    ud.y -= GOAL_INCREMENT
                    ud.yaw = -pi/2
            elif outcome == 'LOW': # Move in the reverse direction
                if ud.prev_move == 'STRAIGHT':
                    ud.x -= GOAL_INCREMENT
                    ud.yaw = pi
                    ud.prev_move = 'BACK'
                elif ud.prev_move == 'BACK':
                    ud.x += GOAL_INCREMENT
                    ud.yaw = 0
                    ud.prev_move = 'STRAIGHT'
                elif ud.prev_move == 'LEFT':
                    ud.y -= GOAL_INCREMENT
                    ud.yaw = -pi/2
                    ud.prev_move = 'RIGHT'
                elif ud.prev_move == 'RIGHT':
                    ud.y += GOAL_INCREMENT
                    ud.yaw = pi/2
                    ud.prev_move = 'LEFT'
            elif outcome == 'SAME': # Choose a new random direction
                ud.prev_move = random.choice(DIRECTIONS)
                rospy.loginfo('Moving %s'%ud.prev_move.lower())
                if ud.prev_move == 'STRAIGHT':
                    ud.x += GOAL_INCREMENT
                    ud.yaw = 0
                elif ud.prev_move == 'BACK':
                    ud.x -= GOAL_INCREMENT
                    ud.yaw = pi
                elif ud.prev_move == 'LEFT':
                    ud.y += GOAL_INCREMENT
                    ud.yaw = pi/2
                elif ud.prev_move == 'RIGHT':
                    ud.y -= GOAL_INCREMENT
                    ud.yaw = -pi/2
            rospy.loginfo('Current Goal (x, y, yaw) = (%f, %f, %f)'%(ud.x, ud.y, ud.yaw))
            return 'succeeded'

        StateMachine.add('GET_GOAL_HIGH', CBState(get_goal, cb_args=['HIGH']),
                         {'succeeded':'MOVE_TO_GOAL'})

        StateMachine.add('GET_GOAL_LOW', CBState(get_goal, cb_args=['LOW']),
                         {'succeeded':'MOVE_TO_GOAL'})

        StateMachine.add('GET_GOAL_SAME', CBState(get_goal, cb_args=['SAME']),
                         {'succeeded':'MOVE_TO_GOAL'})

        StateMachine.add('MOVE_TO_GOAL', move_base.MoveBaseState('/world'),
                         transitions={'succeeded':'LOOP_ASSESSMENT', 'aborted':'aborted'})

        @smach.cb_interface(input_keys=['concentration'],
                            outcomes=['succeeded', 'continue'])

        def count_assess(ud):
            if ud.concentration < MAX_CONCENTRATION:
                return 'continue'
            else:
                return 'succeeded'

        StateMachine.add('LOOP_ASSESSMENT', CBState(count_assess),
                         {'continue':'GET_CONCENTRATION', 'succeeded':'succeeded'})

    return sm0

def callback(msg):
    return True

def main():
    rospy.init_node('smach')

    rospy.wait_for_service('move_base/make_plan') # wait until move_base is ready
    rospy.wait_for_service('plume/plume_simulation_server/get_loggers') # wait until plume server is running (plume is active)

    sub = rospy.Subscriber('/particle_concentration', ChemicalParticleConcentration, callback) # required for SMACH to be able to get single messages

    sm0_executor = find_plume_source()

    intro_server = IntrospectionServer('plume_finder',sm0_executor,'/PLUME_FINDER')
    intro_server.start()

    outcome = sm0_executor.execute()

    rospy.spin()

if __name__ == '__main__':
    main()
