#!/usr/bin/env python

""" This is a task to let the husky move to charge its battery. """

import roslib; roslib.load_manifest('husky_smach')
import rospy

from husky_battery.srv import SetCharging
from std_msgs.msg import Float64

import smach
from smach import Iterator, StateMachine, CBState, Concurrence
from smach_ros import IntrospectionServer, ServiceState, MonitorState

import move_base as move_base
import cv as cv

def go_to_charger():

    sm0 = StateMachine(outcomes=['succeeded','aborted','preempted'])

    sm0.userdata.starting_x = 0
    sm0.userdata.starting_y = 0
    sm0.userdata.starting_yaw = 0

    sm0.userdata.charger_x = 0
    sm0.userdata.charger_y = 0
    sm0.userdata.charger_yaw = 0

    sm0.tag_id = None

    with sm0:

        StateMachine.add('FIND_STARTING_TAG', cv.TagIdentify(),
                         transitions={'succeeded':'SET_STARTING_POSE'})

        StateMachine.add('SET_STARTING_POSE', move_base.TagToStartingPose(rospy.get_param('~starting_locations')),
                         transitions={'succeeded':'FIND_CLOSEST_CHARGING_POINT'})

        StateMachine.add('FIND_CLOSEST_CHARGING_POINT', move_base.FindClosestCharger(rospy.get_param('~charging_locations')),
                         transitions={'succeeded':'MOVE_TO_CHARGER'})

        StateMachine.add('MOVE_TO_CHARGER', move_base.MoveBaseState('/map'),
                         transitions={'succeeded':'START_CHARGING'},
                         remapping={'x':'charger_x',
                                    'y':'charger_y',
                                    'yaw':'charger_yaw'})

        StateMachine.add('START_CHARGING', ServiceState('/battery_client/husky_battery/set_charging', SetCharging,
                         request = True),
                         transitions={'succeeded':'succeeded'})

    return sm0

def monitor_cb(ud, msg):
    if msg.data <= 1e-2:
        return False
    else:
        return True

def child_term_cb(outcome_map):
    if outcome_map['SEEK_CHARGER'] == 'succeeded':
        return True
    elif outcome_map['MONITOR_BATTERY_LEVEL'] == 'invalid':
        rospy.loginfo('The battery has died before reaching a charger')
        return True
    else:
        return False

def out_cb(outcome_map):
    if outcome_map['MONITOR_BATTERY_LEVEL'] == 'invalid':
        return 'failed'
    elif outcome_map['SEEK_CHARGER'] == 'succeeded':
        return 'succeeded'
    elif outcome_map['SEEK_CHARGER'] == 'aborted':
        return 'aborted'
    else:
        return 'failed'

def execute():

    sm1 = Concurrence(outcomes=['succeeded', 'failed', 'aborted', 'preempted'],
                      default_outcome='aborted',
                      child_termination_cb = child_term_cb,
                      outcome_cb = out_cb)

    with sm1:

        Concurrence.add('SEEK_CHARGER', go_to_charger())
        Concurrence.add('MONITOR_BATTERY_LEVEL', MonitorState('husky_battery/charge_level', Float64, monitor_cb))

    return sm1

def main():
    rospy.init_node('smach')
    rospy.wait_for_service('move_base/make_plan')
    sm1_executor = execute()
    intro_server = IntrospectionServer('battery',sm1_executor,'/FIND_CHARGER')
    intro_server.start()
    outcome = sm1_executor.execute()
    rospy.spin()

if __name__ == '__main__':
    main()
