#!/usr/bin/env python

import roslib; roslib.load_manifest('husky_smach')

import rospy
import tf

from scipy.spatial import Voronoi
from math import pi

import shapely.geometry as geo
import numpy as np
import random
import tf

from visualization_msgs.msg import Marker
from geometry_msgs.msg import PolygonStamped, Point, Point32
from husky_msgs.msg import VoronoiArray
from jsk_recognition_msgs.msg import PolygonArray

ANGLES = [0, pi/4, pi/2, 3*pi/4, pi, 5*pi/4, 3*pi/2, 7*pi/4]

def voronoi_finite_polygons_2d(vor, radius=None):

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()

    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))

    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            new_regions.append(vertices)
            continue

        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                continue

            t = vor.points[p2] - vor.points[p1] # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        new_regions.append(new_region.tolist())

    return new_regions, np.array(new_vertices)

def get_marker_msg(x, y, ns, r, g, b):

    mk = Marker()
    mk.header.frame_id = '/map'
    mk.header.stamp = rospy.Time.now()
    mk.ns = ns
    mk.id = 0
    mk.type = 8
    mk.action = 0
    mk.scale.x = 0.2
    mk.scale.y = 0.2
    mk.pose.orientation.w = 1.0
    mk.color.r = r
    mk.color.g = g
    mk.color.b = b
    mk.color.a = 1.0

    for i in range(len(x)):
        p = Point()
        p.x = x[i]
        p.y = y[i]
        p.z = 0
        mk.points.append(p)

    return mk

def get_voronoi(length, width, iterations, no_of_regions, pts_per_region):

    # creating random points across the area
    x = [random.randrange(-length/2, length/2) for i in range(no_of_regions)]
    y = [random.randrange(-width/2, width/2) for i in range(no_of_regions)]
    seed_msg = get_marker_msg(x, y, 'seed_points', 0, 0, 1)
    points = np.array(list(zip(x, y)))

    vor = Voronoi(points, qhull_options='Qbb Qc Qx') # Create the voronoi

    boundary = geo.Polygon([(-width/2, -length/2), (-width/2, length/2), (width/2, length/2), (width/2, -length/2)])
    regions, vertices = voronoi_finite_polygons_2d(vor)

    poly_points = []
    poly_array = PolygonArray()
    poly_array.header.frame_id = '/map'
    poly_array.header.stamp = rospy.Time.now()

    for i in range(iterations):
        for region in regions:

            poly = geo.Polygon(vertices[region])
            poly = poly.intersection(boundary)

            current_poly = PolygonStamped()
            current_poly.header.frame_id = '/map'
            current_poly.header.stamp = rospy.Time.now()

            xx = np.array(poly.exterior.coords.xy)[0]
            yy = np.array(poly.exterior.coords.xy)[1]
            zz = np.array([0 for j in range(xx.size)])

            for k in range(xx.size):
                pt = Point32()
                pt.x = xx[k]
                pt.y = yy[k]
                pt.z = zz[k]
                current_poly.polygon.points.append(pt)

            poly_array.polygons.append(current_poly)

            min_x, min_y, max_x, max_y = poly.bounds
            count = 0

            while count < pts_per_region:
                random_point = geo.Point(random.uniform(min_x, max_x), random.uniform(min_y, max_y))
                if (random_point.within(poly)):
                    poly_points.append(random_point)
                    count += 1

    goal_x = [point.x for point in poly_points]
    goal_y = [point.y for point in poly_points]
    goal_yaw = [random.choice(ANGLES) for point in poly_points]

    return goal_x, goal_y, goal_yaw, seed_msg, poly_array

def main():
    rospy.init_node('voronoi')

    rospy.wait_for_service('move_base/make_plan') # waits for move_base to be active

    map_length = rospy.get_param('~map_length', 18)
    map_width = rospy.get_param('~map_width', 18)
    iterations = rospy.get_param('~iterations', 1)
    no_of_regions = rospy.get_param('~no_of_regions', 5)
    pts_per_region = rospy.get_param('~pts_per_region', 3)

    voronoi_pub = rospy.Publisher('voronoi_points', VoronoiArray, queue_size=1)
    mk_pub = rospy.Publisher('visualization_marker', Marker, queue_size=10)
    poly_pub = rospy.Publisher('voronoi_regions', PolygonArray, queue_size=1)

    points_msg = VoronoiArray()
    points_msg.iterations = iterations
    points_msg.no_of_regions = no_of_regions
    points_msg.pts_per_region = pts_per_region
    (points_msg.x, points_msg.y, points_msg.yaw, seed_msg, poly_array) = get_voronoi(map_length, map_width, iterations, no_of_regions, pts_per_region)

    goals_msg = get_marker_msg(points_msg.x, points_msg.y, 'goal_points', 1, 0, 0)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        voronoi_pub.publish(points_msg)
        mk_pub.publish(seed_msg)
        mk_pub.publish(goals_msg)
        poly_pub.publish(poly_array)
        rate.sleep()

if __name__ == '__main__':
    main()
