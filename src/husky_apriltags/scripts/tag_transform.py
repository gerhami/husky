#!/usr/bin/env python

""" This file creates the transform between the tags and the Husky for localization. """

import roslib; roslib.load_manifest('husky_apriltags')

import rospy
import yaml
import tf

from apriltag_ros.msg import AprilTagDetection
from apriltag_ros.msg import AprilTagDetectionArray
from geometry_msgs.msg import PoseWithCovarianceStamped

from std_msgs.msg import String # for debugging

class tagTransform:
    def parseTagPose(self, file):
        self.id = []
        self.x = []
        self.y = []
        self.z = []
        with open(file,'r') as stream:
            try:
                tags = yaml.safe_load_all(stream)
                for tag in tags:
                    self.id.append(tag['id'])
                    self.x.append(tag['x'])
                    self.y.append(tag['y'])
                    self.z.append(tag['z'])
            except yaml.YAMLError as exc:
                rospy.loginfo('Error parsing tag positions: %s'%exc)
                return

    def callback(self, data, pub):
        msg = PoseWithCovarianceStamped()
        msg.header.stamp = rospy.Time.now()
        if data.detections:
            if data.detections[0].id[0] in self.id:
                msg.pose.pose.position.x = self.x[self.id.index(data.detections[0].id[0])]
                msg.pose.pose.position.y = self.y[self.id.index(data.detections[0].id[0])]
                msg.pose.pose.position.z = self.z[self.id.index(data.detections[0].id[0])]
        pub.publish(msg)

    def __init__(self):
        self.parseTagPose(rospy.get_param('~tag_poses'))
        self.pub = rospy.Publisher('tag_pose', PoseWithCovarianceStamped, queue_size=10)
        rospy.Subscriber('tag_detections', AprilTagDetectionArray, self.callback, self.pub)
        rospy.spin()

if __name__ == '__main__':
    rospy.init_node('tag_transform')
    try:
        tagTransform()
    except rospy.ROSInterruptException:
        pass
