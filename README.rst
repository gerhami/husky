========================================
Husky Repository
========================================

Purpose
+++++++

This Husky repository serves to integrate the existing Clearpath Husky ROS workspace with ROS testing tools - including gtest, rostest, and GitLab CI.

These test suites will continue to be changed and adapted, so refer to the dev branch for current updates.

How to Build
++++++++++++

Create workspace ::

  mkdir -p catkin_ws/src

Copy repository ::

  cd catkin_ws/src
  git clone git@gitlab.com:gerhami/husky.git

Build workspace ::

  cd ..
  catkin build

Run tests ::

  catkin run_tests

or for a single package (for this repo, refer to either the CI Pipelines or the husky_test package) ::

  catkin run_tests [package_name]
